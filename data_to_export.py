from urllib import response
import matplotlib.pyplot as plt
import datetime
from sqlalchemy import create_engine, Table, MetaData
from sqlalchemy.orm import sessionmaker, mapper
import re
import openpyxl
from openpyxl.styles.numbers import BUILTIN_FORMATS
from openpyxl.styles import Alignment
from openpyxl.utils.cell import get_column_letter
import time
import math
import keys
import requests

months = {
    '01': 'Январь',
    '02': 'Февраль',
    '03': 'Март',
    '04': 'Апрель',
    '05': 'Май',
    '06': 'Июнь',
    '07': 'Июль',
    '08': 'Август',
    '09': 'Сентябрь',
    '10': 'Октябрь',
    '11': 'Ноябрь',
    '12': 'Декабрь'
}

    
class NFT():
    def __init__(self, id, title, address, tier, subtier, owner): 
        self.id = id
        self.title = title
        self.address = address
        self.tier = tier
        self.subtier = subtier
        self.owner = owner 
class Trans():
    def __init__(self, source, lt, hash, type, value, utime, nft_address, market): 
        self.source = source
        self.lt = lt
        self.hash = hash
        self.type = type
        self.value = value
        self.utime = utime
        self.nft_address = nft_address
        self.market = market

engine = create_engine(keys.engines['server'])

meta = MetaData(engine)
trans_t = Table('transactions', meta, autoload=True)
mapper(Trans, trans_t)
nft_t = Table('nft_list', meta, autoload=True)
mapper(NFT, nft_t)
Session = sessionmaker(bind=engine)
session = Session()     


def plot_data(x_arg, y_arg, legend='f(x)', title='Plot f(x)', x_label='x', y_label='f(x)'):
    plt.gcf().clear()
    plt.plot(x_arg, y_arg, 'ko-', lw=1.4, mew=2, ms=3)
    plt.legend([legend])
    plt.title(title)
    plt.xlabel(x_label)
    plt.ylabel(y_label)
    plt.grid(True)
    plt.tick_params(axis='x', which='major', labelsize=8)
    plt.savefig(f'grafs/{title}-{legend}')


def dict_create(transactions):
    data = {}
    for i in transactions:
        date = str(datetime.date.fromtimestamp(int(i.utime))).split('-')
        tmp = data.get(date[1], None)
        if tmp == None: data[date[1]] = {date[2]: 0}; continue
        data[date[1]].update({date[2]: tmp.get(date[2], 0)})
    return data


def value_of_day():
    transactions = sorted([i for i in session.query(Trans) if i.type == 'second'], key=lambda x: x.utime)
    data_to_display = dict_create(transactions)

    for i in transactions:
        date = str(datetime.date.fromtimestamp(int(i.utime))).split('-')
        data_to_display[date[1]].update({date[2]: data_to_display.get(date[1], None).get(date[2], 0) + float(i.value)})

    for month, data in data_to_display.items():
        days, values = [], []
        for day, value in data.items(): 
            days.append(day); values.append(round(value, 2))
        plot_data(days, values, 'Сумма', f'Перепродажи NFT за {months[month]}', 'День', 'Сумма')


def quantity_of_day():
    transactions = sorted([i for i in session.query(Trans) if i.type == 'second'], key=lambda x: x.utime)
    data_to_display = dict_create(transactions)

    for i in transactions:
        date = str(datetime.date.fromtimestamp(int(i.utime))).split('-')
        data_to_display[date[1]].update({date[2]: data_to_display.get(date[1], None).get(date[2], 0) + 1})

    for month, data in data_to_display.items():
        days, values = [], []
        for day, value in data.items(): days.append(day); values.append(round(value, 2))
        plot_data(days, values, 'Кол-во', f'Перепродажи NFT за {months[month]}', 'День', 'Количество')
    return data_to_display


def nft_value_of_day():
    transactions = sorted([i for i in session.query(Trans) if i.type == 'second'], key=lambda x: x.utime)
    data_to_display_t1 = dict_create(transactions); data_to_display_t2 = dict_create(transactions); data_to_display_t3 = dict_create(transactions)

    for i in transactions:
        date = str(datetime.date.fromtimestamp(int(i.utime))).split('-')
        nft = session.query(NFT).filter(NFT.address == i.nft_address).one()
        if nft.tier == '1': data_to_display_t1[date[1]].update({date[2]: data_to_display_t1.get(date[1], None).get(date[2], 0) + float(i.value)})
        elif nft.tier == '2': data_to_display_t2[date[1]].update({date[2]: data_to_display_t2.get(date[1], None).get(date[2], 0) + float(i.value)})
        elif nft.tier == '3': data_to_display_t3[date[1]].update({date[2]: data_to_display_t3.get(date[1], None).get(date[2], 0) + float(i.value)})
    for i in data_to_display_t1.keys():
        days = [j for j in data_to_display_t1[i].keys()]
        values_t1, values_t2, values_t3 = [], [], []
        for _, value in data_to_display_t1[i].items(): values_t1.append(round(value, 2))
        for _, value in data_to_display_t2[i].items(): values_t2.append(round(value, 2))
        for _, value in data_to_display_t3[i].items(): values_t3.append(round(value, 2))
        plt.gcf().clear()
        plt.plot(days, values_t1, 'bo-', lw=1.4, mew=2, ms=3, alpha=0.7)
        plt.plot(days, values_t2, 'ko-', lw=1.4, mew=2, ms=3, alpha=0.7)
        plt.plot(days, values_t3, 'ro-', lw=1.4, mew=2, ms=3, alpha=0.7)
        plt.legend(['T1', 'T2', 'T3'])
        plt.title(f'Перепродажи NFT за {months[i]}')
        plt.xlabel('День')
        plt.ylabel('Сумма')
        plt.grid(True)
        plt.tick_params(axis='x', which='major', labelsize=8)
        plt.savefig(f'grafs/Tiers-Перепродажи_NFT_за_{months[i]}_values.png')        


def nft_quantity_of_day():
    transactions = sorted([i for i in session.query(Trans) if i.type == 'second'], key=lambda x: x.utime)
    data_to_display_t1 = dict_create(transactions); data_to_display_t2 = dict_create(transactions); data_to_display_t3 = dict_create(transactions)

    for i in transactions:
        date = str(datetime.date.fromtimestamp(int(i.utime))).split('-')
        nft = session.query(NFT).filter(NFT.address == i.nft_address).one()
        if nft.tier == '1': data_to_display_t1[date[1]].update({date[2]: data_to_display_t1.get(date[1], None).get(date[2], 0) + 1})
        elif nft.tier == '2': data_to_display_t2[date[1]].update({date[2]: data_to_display_t2.get(date[1], None).get(date[2], 0) + 1})
        elif nft.tier == '3': data_to_display_t3[date[1]].update({date[2]: data_to_display_t3.get(date[1], None).get(date[2], 0) + 1})
    for i in data_to_display_t1.keys():
        days = [j for j in data_to_display_t1[i].keys()]
        values_t1, values_t2, values_t3 = [], [], []
        for _, value in data_to_display_t1[i].items(): values_t1.append(round(value))
        for _, value in data_to_display_t2[i].items(): values_t2.append(round(value))
        for _, value in data_to_display_t3[i].items(): values_t3.append(round(value))
        plt.gcf().clear()
        plt.plot(days, values_t1, 'bo-', lw=1.4, mew=2, ms=3, alpha=0.7)
        plt.plot(days, values_t2, 'ko-', lw=1.4, mew=2, ms=3, alpha=0.7)
        plt.plot(days, values_t3, 'ro-', lw=1.4, mew=2, ms=3, alpha=0.7)
        plt.legend(['T1', 'T2', 'T3'])
        plt.title(f'Перепродажи NFT за {months[i]}')
        plt.xlabel('День')
        plt.ylabel('Количество')
        plt.grid(True)
        plt.tick_params(axis='x', which='major', labelsize=8)
        plt.savefig(f'grafs/Tiers-Перепродажи_NFT_за_{months[i]}_quantity.png') 


def to_table_of_trans(type='second'):
    result = []
    trans = sorted([i for i in session.query(Trans) if i.type == type], key=lambda x: x.utime)
    tmp = {}
    for i in trans:
        nft = session.query(NFT).filter(NFT.address == i.nft_address).one()
        tmp.update({nft.address: tmp.get(nft.address, 0) + 1})
    for i in trans:
        nft = session.query(NFT).filter(NFT.address == i.nft_address).one()
        date = str(datetime.date.fromtimestamp(int(i.utime)))
        result.append([date, float(i.value) , int(nft.id) , nft.title.rstrip() , int(nft.tier) , nft.address , i.market, tmp[i.nft_address]])
    return result


def exception_handler(request, exception):
    print('Request failed')
    

def get_owner(address_nft):
    url = f'https://toncenter.com/api/v2/getTransactions?address={address_nft}&api_key={keys.api_to_ton}&limit=100&to_lt=0&archival=true'
    ignore_list = ['EQCjk1hh952vWaE9bRguFkAhDAL5jj3xj9p0uPWrFBq_GEMS', 'EQDrLq-X6jKZNHAScgghh0h1iog3StK71zn8dcmrOj8jPWRA']
    try:
        responce = requests.get(url)
    except Exception:
        time.sleep(5); print('Соединение с сетью потеряно'); return get_owner(address_nft)
    if responce.status_code == 200:
        utime = 0
        owner = None
        for i in responce.json()['result']:
            if len(i['out_msgs']) > 0:
                for ms in i['out_msgs']:
                    if i['utime'] > utime and ms['destination'] not in ignore_list:
                        owner = ms['destination']
                        utime = i['utime']
        return owner
    else: time.sleep(0.3); return get_owner(address_nft)


def to_table_of_nft():
    result = []
    for i in sorted([i for i in session.query(NFT)], key=lambda x: x.id):
        title = re.sub(r"[^a-zA-Z0-9 ]", "", i.title)
        st = f'{str(i.id)};{title};{i.tier};{i.subtier};{i.address};{i.owner};'
        ab_trans = session.query(Trans).filter(Trans.nft_address == i.address).all()
        for j in ab_trans:
            date = str(datetime.date.fromtimestamp(int(j.utime)))
            st = st + f'{date};{j.value};{j.market};'
        result.append(st.split(';'))
    return result


def to_exel():
    all_trans = to_table_of_trans()
    about_nft = to_table_of_nft()
    
    book = openpyxl.Workbook()
    book.remove(book.active)
    book.create_sheet('All trans', 0)
    book.create_sheet('About NFT', 1)
    
    max_trans = 0
    for row in about_nft:
        book.worksheets[1].append(row)
    for row in all_trans:
        trans = [i[6:] for i in about_nft if str(i[0]) == str(row[2])] 
        trans[0][1::3] = [float(i) for i in trans[0][1::3]]
        book.worksheets[0].append(row + trans[0])
        if len(trans[0]) / 3 > max_trans: max_trans = len(trans[0])

    sheet_trans = book.worksheets[0]
    
    for i in sheet_trans['B']: i.number_format = BUILTIN_FORMATS[2]
    for i in [sheet_trans['C'], sheet_trans['E'], sheet_trans['H']]:
        for j in i: j.number_format = BUILTIN_FORMATS[1]
    
    sheet_trans.insert_rows(0)
    sheet_trans['A1'].value = 'Дата';
    sheet_trans['B1'].value = 'Сумма'
    sheet_trans['C1'].value = 'ID NFT'
    sheet_trans['D1'].value = 'Name NFT';
    sheet_trans['E1'].value = 'Tier NFT'
    sheet_trans['F1'].value = 'Address NFT'
    sheet_trans['G1'].value = 'Market'
    sheet_trans['H1'].value = 'Resales NFT'
    sheet_trans.column_dimensions['A'].width = 10
    sheet_trans.column_dimensions['B'].width = 10
    sheet_trans.column_dimensions['C'].width = 6
    sheet_trans.column_dimensions['D'].width = 42
    sheet_trans.column_dimensions['E'].width = 8
    sheet_trans.column_dimensions['F'].width = 60
    sheet_trans.column_dimensions['G'].width = 15
    sheet_trans.column_dimensions['H'].width = 11
    
    
    start_column = 9
    for i in range(max_trans):
        sheet_trans.column_dimensions[f'{get_column_letter(start_column)}'].width = 12
        sheet_trans.column_dimensions[f'{get_column_letter(start_column+1)}'].width = 10
        sheet_trans.column_dimensions[f'{get_column_letter(start_column+2)}'].width = 12
        for i in sheet_trans[f'{get_column_letter(start_column+1)}']: i.number_format = BUILTIN_FORMATS[2]
        start_column += 3

    
    sheet = book.worksheets[1]
    sheet.insert_rows(0)
    sheet['A1'].value = 'ID';
    sheet['B1'].value = 'Name'
    sheet['C1'].value = 'Tier'
    sheet['D1'].value = 'Subtier';
    sheet['E1'].value = 'Address'
    sheet['F1'].value = 'Owner'
    sheet.column_dimensions['A'].width = 5.29
    sheet.column_dimensions['B'].width = 58.71
    sheet.column_dimensions['C'].width = 5
    sheet.column_dimensions['D'].width = 10
    sheet.column_dimensions['E'].width = 60.3
    sheet.column_dimensions['F'].width = 60.3
    
    start_column = 6
    for i in range(max_trans):
        sheet_trans.column_dimensions[f'{get_column_letter(start_column)}'].width = 12
        sheet_trans.column_dimensions[f'{get_column_letter(start_column+1)}'].width = 10
        sheet_trans.column_dimensions[f'{get_column_letter(start_column+2)}'].width = 12
        for i in sheet_trans[f'{get_column_letter(start_column+1)}']: i.number_format = BUILTIN_FORMATS[2]
        start_column += 3
    
    book.save(f'result.xlsx')


def check_summ(number):
    if math.isclose(number, 51, rel_tol=0.1) or math.isclose(number, 151, rel_tol=0.1) or math.isclose(number, 1251, rel_tol=0.1): return True
    else: return False


def excel_first():
    trans = sorted([i for i in session.query(Trans) if i.type == 'first'], key=lambda x: x.utime)

    quantity_t1 = dict_create(trans); values_t1 = dict_create(trans)
    quantity_t2 = dict_create(trans); values_t2 = dict_create(trans)
    quantity_t3 = dict_create(trans); values_t3 = dict_create(trans)
    for i in trans:
        if not check_summ(float(i.value)): continue
        date = str(datetime.date.fromtimestamp(int(i.utime))).split('-')
        nft = session.query(NFT).filter(NFT.address == i.nft_address).one()
        if nft.tier == '1': 
            quantity_t1[date[1]].update({date[2]: quantity_t1.get(date[1], None).get(date[2], 0) + 1})
            values_t1[date[1]].update({date[2]: values_t1.get(date[1], None).get(date[2], 0) + float(i.value)})
        elif nft.tier == '2':
            quantity_t2[date[1]].update({date[2]: quantity_t2.get(date[1], None).get(date[2], 0) + 1})
            values_t2[date[1]].update({date[2]: values_t2.get(date[1], None).get(date[2], 0) + float(i.value)})
        elif nft.tier == '3':
            quantity_t3[date[1]].update({date[2]: quantity_t3.get(date[1], None).get(date[2], 0) + 1})
            values_t3[date[1]].update({date[2]: values_t3.get(date[1], None).get(date[2], 0) + float(i.value)})
    res = []
    for month, data in values_t1.items():
        for day, value in data.items():
            res.append([months[month], int(day), 1, quantity_t1[month][day], float(values_t1[month][day])])
            res.append([months[month], int(day), 2, quantity_t2[month][day], float(values_t2[month][day])])
            res.append([months[month], int(day), 3, quantity_t3[month][day], float(values_t3[month][day])])
    
    book = openpyxl.Workbook()
    book.remove(book.active)
    book.create_sheet('First sales', 0)
    for i in res: book.worksheets[0].append(i)

    sheet_trans = book.worksheets[0]
    for i in sheet_trans['E']: i.number_format = BUILTIN_FORMATS[2]
    for i in [sheet_trans['B'], sheet_trans['C'], sheet_trans['D']]:
        for j in i: j.number_format = BUILTIN_FORMATS[1]
    sheet_trans.insert_rows(0)
    sheet_trans['A1'].value = 'Месяц'
    sheet_trans['B1'].value = 'День'
    sheet_trans['C1'].value = 'Тир'
    sheet_trans['D1'].value = 'Количество'
    sheet_trans['E1'].value = 'Сумма'
    sheet_trans.column_dimensions['A'].width = 10
    sheet_trans.column_dimensions['B'].width = 7
    sheet_trans.column_dimensions['C'].width = 6
    sheet_trans.column_dimensions['D'].width = 11
    sheet_trans.column_dimensions['E'].width = 10
    book.save(f'result_first.xlsx')


def two_weeks():
    all_trans = sorted([i for i in session.query(Trans) if i.type == 'second'], key=lambda x: x.utime)
    dt = datetime.date.fromtimestamp(int(time.time() - 1*60*60*24*14))
    trans = [i for i in all_trans if datetime.date.fromtimestamp(int(i.utime)) >= dt]
    markets = []; [markets.append(i.market) for i in trans if i.market not in markets]
    res = {str(datetime.date.fromtimestamp(int(i.utime))): {j: {'value': 0.0, 'quantity': 0, 'maxes':{'price': 0, 'nft': 'None'}} for j in markets} for i in trans}
    for i in trans:
        date = str(datetime.date.fromtimestamp(int(i.utime)))
        res[date][i.market]['value'] += float(i.value)
        res[date][i.market]['quantity'] += 1
        if res[date][i.market]['maxes']['price'] < float(i.value): 
            res[date][i.market]['maxes']['price'] = float(i.value)
            res[date][i.market]['maxes']['nft'] = session.query(NFT).filter(NFT.address == i.nft_address).one()
    rows = []
    for date, data in res.items():
        for market, data2 in data.items(): 
            if data2['maxes']['nft'] != 'None': msg = '=HYPERLINK("{}", "{}")'.format('https://tonscan.org/nft/{}'.format(data2['maxes']['nft'].address),  data2['maxes']['nft'].title)
            else: msg = 'None'                                                                
            rows.append([date, market, data2['value'], data2['quantity'], msg, data2['maxes']['price']])
    
    book = openpyxl.Workbook()
    book.remove(book.active)
    book.create_sheet('Two weeks', 0)
    for i in rows: book.worksheets[0].append(i)
    sheet_trans = book.worksheets[0]
    for i in sheet_trans['D']: i.number_format = BUILTIN_FORMATS[1]
    for i in [sheet_trans['C'], sheet_trans['F']]:
        for j in i: j.number_format = BUILTIN_FORMATS[2]
    sheet_trans.insert_rows(0)
    sheet_trans['A1'].value = 'Дата'
    sheet_trans['B1'].value = 'Маркет'
    sheet_trans['C1'].value = 'Сумма'
    sheet_trans['D1'].value = 'Кол-во'
    sheet_trans.column_dimensions['A'].width = 10
    sheet_trans.column_dimensions['B'].width = 10
    sheet_trans.column_dimensions['C'].width = 10
    sheet_trans.column_dimensions['D'].width = 8
    sheet_trans.column_dimensions['E'].width = 32
    sheet_trans.column_dimensions['F'].width = 10
    sheet_trans.merge_cells('E1:F1')
    sheet_trans['E1'].value = 'Максимальная продажа'
    book.save(f'two_week.xlsx')
        
if __name__ == '__main__':
    # _nft = [i for i in session.query(NFT) if i.address == 'EQAsBsu2T_eXsCsOW80v-VHMaH_nkfYSictZ7oLEhCOG3buB']
    # # rare > test
    # _nft[0].subtier = 'Rare'
    # session.commit()
    
    # st = time.time()
    # to_exel()
    # print(time.time() - st)
    exit()
    value_of_day()
    quantity_of_day()
    nft_quantity_of_day()
    nft_value_of_day()
