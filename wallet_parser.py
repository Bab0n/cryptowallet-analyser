import requests
import time
from sqlalchemy import create_engine, Table, Column, MetaData, String
from sqlalchemy.orm import sessionmaker, mapper
import data_to_export
import keys


def db_create():
    transactions = Table('transactions', meta,
        Column('source', String, primary_key=True),
        Column('lt', String, nullable=False),
        Column('hash', String, nullable=False),
        Column('type', String, nullable=False),
        Column('value', String, nullable=False),
        Column('utime', String, nullable=False),
        Column('nft_address', String, nullable=False),
        Column('market', String, nullable=False)
        )
    meta.create_all(engine)


class NFT():
    def __init__(self, id, title, address, tier, subtier, owner): 
        self.id = id
        self.title = title
        self.address = address
        self.tier = tier
        self.subtier = subtier
        self.owner = owner 
class Trans():
    def __init__(self, source, lt, hash, type, value, utime, nft_address, market): 
        self.source = source
        self.lt = lt
        self.hash = hash
        self.type = type
        self.value = value
        self.utime = utime
        self.nft_address = nft_address
        self.market = market

f_trans = Trans('EQDJsGIgU2gW9GsIh0Y9RGIryXQvI4VjpHZVPzTBdqd8UxvV', '27479388000005', 'Nc4qPznJJHMiKYiJbrA9fuma9xyA5ANJ9nwE+u5JauY=',\
    'first', '49.9999500009', '1651163411', 'EQCjmeYIysYAdJmIKFArhoeK9zm3UALMgmveb1uMyRlmzKYD', 'Unknown')


address, key = 'EQANKN8ZnM0OzYOENTkOEg7VVgFog5fBWdCtqQro1MRmU5_2', keys.api_to_ton
headers = {
    'accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9',
    'user-agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/99.0.4844.84 Safari/537.36 OPR/85.0.4341.72'
}
markets = {
    'EQDrLq-X6jKZNHAScgghh0h1iog3StK71zn8dcmrOj8jPWRA': 'Disintar',
    'EQCjk1hh952vWaE9bRguFkAhDAL5jj3xj9p0uPWrFBq_GEMS': 'GetGems'
}

def cicle_responce(url: str, retry=5):
    try:
        response = requests.get(url=url, headers=headers)
        if response.status_code == 504: time.sleep(2); return(cicle_responce(url, retry=(retry-1)))
    except Exception as ex:
        if retry:
            print(f'[INFO] retry={retry} => {url}')
            time.sleep(2)
            return cicle_responce(url, retry=(retry-1))
        else:
            return None
    else:
        return response


def get_info_of_nft(adres: str):
    resp1 = cicle_responce(f"{'https://api.ton.cat/v2/explorer/nft/address/' + adres}")
    return resp1.json()


def get_last_transaction(adres: str) -> list:
    try:
        responce = (get_address_info(adres))['result']['last_transaction_id']
        return [responce['lt'], responce['hash']]
    except Exception as ex: time.sleep(5); print('Произошла ошибка при получении последней транзакции'); return get_last_transaction(adres)


def get_transactions(start_lt: int, hash_first_trans: str, finish_lt: int = 0, limit: int = 5, adres = address):
    url = 'https://toncenter.com/api/v2/getTransactions'
    query = {'address':adres, 'api_key':key, 'lt':start_lt, 'hash':hash_first_trans, 'to_lt':finish_lt, 'archival':True, 'limit':limit}
    try:
        responce = requests.get(url, params=query)
    except ConnectionError:
        time.sleep(5); print('Соединение с сетью потеряно'); return get_transactions(str(start_lt), str(hash_first_trans), str(finish_lt), adres=adres)
    if responce.status_code == 500: return get_transactions(str(start_lt), str(hash_first_trans), str(finish_lt), adres=adres)
    elif responce.status_code == 200: return responce.json()
    else: time.sleep(2); return get_transactions(start_lt, hash_first_trans, finish_lt, adres=adres)


def get_address_info(adres):
    try:
        url = 'https://toncenter.com/api/v2/getAddressInformation'; query = {'address':adres, 'api_key':key}
        responce = requests.get(url, params=query)
        if responce.status_code == 200: return responce.json()
        else: time.sleep(1); return get_address_info(adres)
    except Exception as ex: time.sleep(2); return get_address_info(adres)


def getDirectionTrans(transaction) -> str:
    if transaction['in_msg']['source'] == '': return 'OUT'
    else: return 'IN'


def haveMessage(transaction) -> bool:
    if transaction['in_msg']['message'] == '': return False
    else: return True


def update_owner(nft, owner):
    pass
  
def database_update():
    last_trans = get_last_transaction(address)
    sourcss = [j.source for j in session.query(Trans)]
    print('Check to have new transactions')
    def ps1(lt_start, hash_start, sourses, ltss, hashss, utimess, valuess):
        resp = get_transactions(lt_start, hash_start, '27479388000005', limit=2000)
        for i in resp['result']:
            if int(i['utime']) >= int('1651163411'):
                if getDirectionTrans(i) == 'OUT' or haveMessage(i): continue
                if i['in_msg']['source'] in sourcss: continue
                sourses.append(i['in_msg']['source']); ltss.append(i['transaction_id']['lt']); hashss.append(i['transaction_id']['hash']); 
                utimess.append(i['utime']); valuess.append(float(i['in_msg']['value']))
        if len(resp['result']) < 2000: 
            return sourses, ltss, hashss, utimess, valuess
        return ps1(ltss[-1], hashss[-1], sourses, ltss, hashss, utimess, valuess)

    sources, lts, hashs, utimes, values = ps1(last_trans[0], last_trans[1], [], [], [], [], [])
    if len(sources) == 0: print('Not have new transactions'); return
    
    print('New transactions findet! Start adding')
    _temp_ = {}
    for i in range(len(sources)):
        sourc = sources[i]
        print(f'[{i+1}/{len(sources)}] | From: {sourc}')
        _lt = get_last_transaction(sourc)
        nft = get_transactions(_lt[0], str(_lt[1]), finish_lt=1, limit=100, adres=sourc)
        for tpm1 in nft['result']:
            if len(tpm1['out_msgs']) != 4: continue
            value = 0
            market = 'Unknown'
            for tpm2 in tpm1['out_msgs']:
                match tpm2['destination']:
                    case 'EQCjk1hh952vWaE9bRguFkAhDAL5jj3xj9p0uPWrFBq_GEMS': market = 'GetGems'
                    case 'EQDrLq-X6jKZNHAScgghh0h1iog3StK71zn8dcmrOj8jPWRA': market = 'Disintar'
                    
                val_ = float(tpm2['value']) / 1000000000
                value += val_
                if val_ < 2: nft = tpm2['destination']         
            if type(nft) != type('1'): continue
            if sources.count(sources[i]) == 2:
                _temp_[sourc] = {'lt':lts[i], 'hash':hashs[i], 'value': value, 'utime': utimes[i], 'type':'first', 'nft':nft, 'market':market}
            elif sources.count(sources[i]) == 1:
                _temp_[sourc] = {'lt':lts[i], 'hash':hashs[i], 'value': value, 'utime': utimes[i], 'type':'second', 'nft':nft, 'market':market}
    transactions = {}
    [transactions.update({k:v}) for k,v in _temp_.items() if k not in transactions.keys()]
    for z, b in transactions.items():
        try:
            new_trans = Trans(str(z), b['lt'], b['hash'], b['type'], str(b['value']), b['utime'], b['nft'], b['market'])
            _nft = [i for i in session.query(NFT) if i.address == new_trans.nft_address]
            new_own = data_to_export.get_owner(new_trans.nft_address)
            _nft[0].owner = new_own
            session.add(new_trans)
            session.commit()
            print(f'{new_trans.nft_address} | Own: {_nft[0].owner}')
        except Exception as e:
            print(f"[-] Error update database: {e}")
    print('Database updated')


if __name__ == '__main__':
    engine = create_engine(keys.engines['server'])
    
    meta = MetaData(engine)
    db_create()
    trans_t = Table('transactions', meta, autoload=True)
    mapper(Trans, trans_t)
    nft_t = Table('nft_list', meta, autoload=True)
    mapper(NFT, nft_t)
    Session = sessionmaker(bind=engine)
    session = Session() 

    while True:
        database_update()
        print('Начало сборки таблиц Excel')
        data_to_export.to_exel()
        data_to_export.excel_first()
        data_to_export.two_weeks()
        print('Сохранение графиков')
        data_to_export.value_of_day()
        data_to_export.quantity_of_day()
        data_to_export.nft_quantity_of_day()
        data_to_export.nft_value_of_day()
        print('> Процесс окончен. Timout 600 sec')
        time.sleep(600)
