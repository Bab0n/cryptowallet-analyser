from aiogram import Bot, Dispatcher, executor, types
import aiogram
from aiogram.contrib.fsm_storage.memory import MemoryStorage
import keys
import os
import data_to_export

main_kb = aiogram.types.ReplyKeyboardMarkup(resize_keyboard=True)
main_kb.row(
    aiogram.types.KeyboardButton('Первичные'),
    aiogram.types.KeyboardButton('Вторичные')
)
main_kb.row(
    aiogram.types.KeyboardButton('Графики'),
    aiogram.types.KeyboardButton('2 недели')
)


bot = Bot(token=keys.token)
storage = MemoryStorage()
dp = Dispatcher(bot, storage=storage)

@dp.message_handler(commands=['getaccess'])
async def get_pass(message: types.Message):
    match message.text.split():
        case [cmd, (keys.key)]:
            with open('users', 'a') as f: print(f'{message.from_user.id};', file=f, end="")
            await message.answer('Вы получили доступ к статистике!', reply_markup=main_kb)
        case [cmd]: await message.answer('Не указан ключ доступа.\nВведите - /help')
        case _: await message.answer('Введён <b>неверный</b> ключ доступа', parse_mode='HTML')

@dp.message_handler(aiogram.dispatcher.filters.Text(equals=['Первичные', 'Вторичные', 'Графики', '2 недели']))
async def test(message : types.Message):
    with open('users', 'r') as f: 
        usrs = [i.split(';') for i in f.readlines()]
        if len(usrs) != 0 and str(message.from_user.id) in usrs[0]: pass
        else: await message.answer('Вы не имеете доступа к статистике\nВведите - /help', reply_markup=types.ReplyKeyboardRemove()); await message.delete(); return
    match message.text:
        case 'Вторичные': await message.answer_document(open('result.xlsx', 'rb'))
        case '2 недели': await message.answer_document(open('two_week.xlsx', 'rb'))
        case 'Графики':  [await message.answer_photo(open(f'grafs/{i}', 'rb')) for i in os.listdir('grafs')]
        case 'Первичные': await message.answer_document(open('result_first.xlsx', 'rb'))
    await message.delete()

@dp.message_handler(commands=['start', 'help'])
async def start_cmd(message : types.Message):
    await message.answer("""
Данный бот представляет статистику для владельца коллекции
<b>Animals Red List</b>
Если вы обладаете ключом доступа, то авторизуйтесь в боте с помощью
<pre><strong>/getaccess</strong> KEY</pre>
""", parse_mode='HTML')

    

if __name__ == '__main__':
    if not os.path.exists('users'):
        with open('users', 'w') as f: pass
    executor.start_polling(dp, skip_updates=True)