import requests
import time
from sqlalchemy import create_engine, Table, Column, Integer, Boolean, MetaData, String
from bs4 import BeautifulSoup as BS
from sqlalchemy.orm import sessionmaker, mapper


def db_create():
    users = Table('nft_list', meta, 
        Column('id', Integer, primary_key=True),
        Column('title', String, nullable=False),
        Column('address', String, nullable=False),
        Column('tier', String, nullable=False),
        Column('subtier', String, nullable=False),
        Column('owner', String, nullable=False)
        )
    meta.create_all(engine)
    
    
class NFT():
    def __init__(self, id, title, address, tier, subtier, owner): 
        self.id = id; self.title = title; self.address = address;self.tier = tier; self.subtier = subtier; self.owner = owner
    
    
    
headers = {
    'accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9',
    'user-agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/99.0.4844.84 Safari/537.36 OPR/85.0.4341.72'
}   
def get_resp(url, retry=5):
    try:
        response = requests.get(url=url, headers=headers)
        print(f"[+] Responce code: {response.status_code}")
        if response.status_code == 504: time.sleep(2); return(get_resp(url, retry=(retry-1)))
    except Exception as ex:
        if retry:
            print(f'[INFO] retry={retry} => {url}')
            time.sleep(2)
            return get_resp(url, retry=(retry-1))
        else:
            return None
    else:
        return response
    

def tiers():
    i_d_s = [i.id for i in session.query(NFT)]
    for i in i_d_s:           
        nft = session.query(NFT).get(i)

        if nft.tier == '':
            resp1 = get_resp(url=('https://api.ton.cat/v2/explorer/nft/address/' + nft.address))
            if resp1.json()['type'] == None:
                time.sleep(10)
                continue
            mass = resp1.json()['content']['attributes']  
            temp = [[f'{mass[0]["trait_type"]}'.lower(), f'{mass[0]["value"]}'], [f'{mass[1]["trait_type"]}'.lower(), f'{mass[1]["value"]}']]
            print(f'[=|{i}|=]')
            for j in temp:
                if j[0] == 'tier': nft.tier = j[1];session.commit()
                elif j[0] == 'subtier': nft.subtier = j[1];session.commit()   
    
def main():
    tiers(); return
    with open('pkl.html', 'r', encoding='utf-8') as f:
        html = BS(f.read(), 'html.parser')
    
    ids = [(i.text.split())[1] for i in html.select('.nft-preview__meta')]
    titles = [i.text for i in html.select('.nft-preview__title')]
    addreses = [(i.get('href').split('/'))[2] for i in html.find_all('a')]
    for i in range(len(ids)):
        new_nft = NFT(ids[i], titles[i], addreses[i], '', '')
        session.add(new_nft)
        session.commit()

if __name__ == "__main__":
    engine = create_engine('postgresql+psycopg2://postgres:1@127.0.0.1:5432/nft_info')
    meta = MetaData(engine)
    Session = sessionmaker(bind=engine)
    session = Session()
    db_create()
    nft_t = Table('nft_list', meta, autoload=True)
    mapper(NFT, nft_t)
    #main()
